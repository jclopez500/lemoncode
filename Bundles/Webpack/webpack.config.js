require('dotenv').config();

const path = require("path");
const glob = require("glob");

const merge = require("webpack-merge");
const parts = require("./webpack.parts");

const HtmlWebpackPlugin = require("html-webpack-plugin");

const PATHS = {
    app: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'dist'),
  };
  

//------------------------------------------------------------

const commonConfig = merge([

    parts.clean( PATHS.build ),
    parts.loadSCSS(),
    parts.loadImages({
        limit: 15000,
        name: "./images/[name].[hash:4].[ext]",
    }),
    parts.CompressImages(),
    parts.loadJavaScript(),
    parts.loadTypeScript(),
    parts.loadFonts(),
    parts.setFreeVariable("BTN_TEXT", "Click Me"),


    {
        entry: {
            app: PATHS.app
        },
        output: {
            path: PATHS.build,
            chunkFilename: "[name].[chunkhash:4].js",
            filename: "[name].[chunkhash:4].js",
          },
    },

    //Write Index.html file
    {
        plugins: [
            new HtmlWebpackPlugin({
                title: "Webpack demo",
            }),
        ],
    },

]);

//------------------------------------------------------------

const productionConfig = merge([

    // Automation splitting
    {
        optimization: {
            splitChunks: {
                chunks: "initial",
            },
            runtimeChunk: {
                name: "manifest",
            },
        },
    },

    parts.extractCSS({
        use: "css-loader",
    }),

    parts.purifyCSS({
        paths: glob.sync(`${PATHS.app}/**/*.js`, { nodir: true }),
    }),

    parts.generateSourceMaps({ type: "source-map" }),

    parts.attachRevision(),

    parts.minifyJavaScript(),

    parts.minifyCSS({
      options: {
        discardComments: {
          removeAll: true,
        },
        safe: true,
      },
    }),

    parts.stats(),



]);

//------------------------------------------------------------

const developmentConfig = merge([

  parts.devServer({
    // Customize host/port here if needed
    host: process.env.HOST,
    port: process.env.PORT,
  }),

  parts.loadCSS(),

]);

//------------------------------------------------------------

module.exports = mode => {

  const config = mode === "production" ? productionConfig : developmentConfig;

  return merge(commonConfig, config, { mode });
};


