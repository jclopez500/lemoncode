



//---------------------------------------------------------------

exports.newImage = (image) => {
    const img = document.createElement('img');
    img.src = image;
    return img;
}

//---------------------------------------------------------------

exports.newDiv = () => {
    const div = document.createElement("div");
    return div;
  };

//---------------------------------------------------------------

exports.newP = (text,style) => {
    const p = document.createElement("p");
    p.innerHTML = text;
    p.classList.add(style);
    return p;
  };

//---------------------------------------------------------------

exports.newButton = (text, style) => {
    const btn = document.createElement('button');
    btn.innerHTML = text;
    btn.classList.add(style);
    return btn
};
