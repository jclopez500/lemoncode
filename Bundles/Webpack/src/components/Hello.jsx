
import React from 'react';
import AmaranthRegularFontFamily from '../fonts/Amaranth-Regular.ttf'

//-------------------------------------

const divStyle = {
  color: 'brown',
  fontFamily : AmaranthRegularFontFamily,
};
    
export const Hello = () => (
  <h1 style={divStyle}>Hello from Component React</h1>
);

//-------------------------------------


