
 
 
 const btnComponent = (text = BTN_TEXT) => {
    const btn = document.createElement("button");

    // div.setAttribute("style", "border-radius: 2px; background-color: green; color: white; width: 10vw; margin-top:5px");
    btn.setAttribute("class", "button-color--orange");
    btn.innerHTML = text;
  
    btn.onclick = () =>
      import("../js/lazy")
        .then(lazy => {
          btn.textContent = lazy.default;
        })
        .catch(err => {
          console.error(err);
        });
  
  
    return btn;
  };

  export default btnComponent;
  