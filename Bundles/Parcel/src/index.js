import lemonLogo from './images/logo_1.png'; //image
import './scss/style.scss'; //sass
import "./css/main.css"; //styles
import tb from './js/toolbox.js'; //javascript
import btnComponent from  './components/btn.js'; //javascript
import { Sum } from './ts/service.ts'; //typescript

//react
import React from 'react';
import { render } from 'react-dom';
import { Hello } from './components/Hello.jsx';
 
//----------------------------------------------------

const append = (e) => document.body.appendChild(tb.newDiv()).appendChild(e);

append(tb.newP(`Altogether is ${ Sum(10,134) }`));
append(tb.newButton('Button', 'button-color--lime'));
append(tb.newP(''));
append(tb.newImage(lemonLogo,'lemonLogo',200));
render(<Hello />, append(tb.newDiv()));

const msg = `Click the button bellow and the button's text will be replaced by a lazy module`;
append(tb.newP(msg));
append(btnComponent());


