
import React from 'react';
import '../css/main.css';

//-------------------------------------

const divStyle = {
  color: 'brown',
  fontFamily : 'MyWebFont'
};
    
export const Hello = () => (
  <h1 style={divStyle}>Hello from Component React</h1>
);

//-------------------------------------


