import { actionsEnums } from '../common/actionsEnums';
import { MemberEntity } from '../model/member';

//--------------------

const initialMemberState = {
  members: <MemberEntity[]>[],
  error: ''
}

//--------------------

export const memberReducer = (state = initialMemberState, action: any) => {
  switch (action.type) {
    case actionsEnums.MEMBER_REQUEST_COMPLETED:
      {
        return Object.assign({}, state, { members: action.payload, error: '' });
      }
    case actionsEnums.ERROR:
      {
        console.log('[payload]', action.payload)
        return Object.assign({}, state, { members: [], error: action.payload });
      }
  }

  return state;
};



