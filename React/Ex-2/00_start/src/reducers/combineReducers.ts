import { combineReducers } from 'redux';
import { memberReducer } from './memberReducer';
import { organizationNameReducer } from './organizationReducer';


export const reducers = combineReducers({
  memberReducer,
  organizationNameReducer
});
