
import { actionsEnums } from '../common/actionsEnums';
import { memberAPI } from '../api/member'


//--------------------

export function setCurrentPage(page: number) {
  return {
    type: actionsEnums.SET_CURRENT_PAGE,
    payload: page
  }
}

//--------------------

export function setOrganizationName(organizationName: string) {
  return {
    type: actionsEnums.SET_ORGANIZATION_NAME,
    payload: organizationName
  }
}

//--------------------

export function memberRequest(organizationName: string) {
  return function (dispatch: any) {
    return memberAPI.getAllMembers(organizationName)
      .then(members => {
        dispatch({
          type: actionsEnums.MEMBER_REQUEST_COMPLETED,
          payload: members
        });
      })
      .catch(error => {
        dispatch({
          type: actionsEnums.ERROR,
          payload: error
        });
      });
  };
}
