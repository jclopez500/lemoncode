export interface MemberDetails {
  id: number;
  login: string;
  name: string;
  company: string;
  location: string;
  followers: number;
  following: number;
}

//--------------

export interface MemberEntity {
  id: number;
  login: string;
  avatar_url: string;
}

//--------------

export const createDefaultMemberEntity = () => ({
  id: -1,
  login: '',
  avatar_url: '',
});

//--------------

export const createDefaultMemberDeatilsEntity = () => ({
  id: -1,
  login: '',
  name: '',
  company: '',
  location: '',
  followers: 0,
  following: 0,
});
