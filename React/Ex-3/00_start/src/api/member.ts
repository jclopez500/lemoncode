import { MemberDetails, MemberEntity, createDefaultMemberEntity, createDefaultMemberDeatilsEntity } from '../model/member';

class MemberAPI {

  //----------------

  getMemberByUser(login: string): Promise<MemberDetails> {
    const gitHubMembersUrl: string = `https://api.github.com/users/${login}`;

    return fetch(gitHubMembersUrl)
      .then((response) => this.checkStatus(response))
      .then((response) => this.parseJSON(response))
      .then((data) => this.resolveMembersDetails(data))

  }

  //----------------

  // Just return a copy of the mock data
  getAllMembers(organizationName: string): Promise<MemberEntity[]> {
    const gitHubMembersUrl: string = `https://api.github.com/orgs/${organizationName}/members`;

    return fetch(gitHubMembersUrl)
      .then((response) => this.checkStatus(response))
      .then((response) => this.parseJSON(response))
      .then((data) => this.resolveMembers(data))
  }

  //----------------

  private checkStatus(response: Response): Promise<Response> {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response);
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }

  //----------------

  private parseJSON(response: Response): any {
    return response.json();
  }

  //----------------

  private resolveMembers(data: any): Promise<MemberEntity[]> {

    const members = data.map((gitHubMember: any) => {
      var member: MemberEntity = createDefaultMemberEntity();

      member.id = gitHubMember.id;
      member.login = gitHubMember.login;
      member.avatar_url = gitHubMember.avatar_url;

      return member;
    });

    return Promise.resolve(members);
  }

  //----------------

  private resolveMembersDetails(data: any): Promise<MemberDetails> {

    let member: MemberDetails = createDefaultMemberDeatilsEntity();

    member.id = data.id;
    member.login = data.login;
    member.name = data.name;
    member.company = data.company;
    member.location = data.location;
    member.followers = data.followers;
    member.following = data.following;

    return Promise.resolve(member);
  }
}

export const memberAPI = new MemberAPI();
