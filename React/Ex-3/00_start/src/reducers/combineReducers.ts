import { combineReducers } from 'redux';
import { memberReducer } from './memberReducer';
import { organizationNameReducer } from './organizationReducer';
import { pageReducer } from './pageReducer';


export const reducers = combineReducers({
  memberReducer,
  organizationNameReducer,
  pageReducer
});
