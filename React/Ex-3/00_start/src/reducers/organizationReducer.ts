import { actionsEnums } from '../common/actionsEnums';

//--------------------

const initialOrganizationNameState = {
    organizationName: ''
}

//--------------------

export const organizationNameReducer = (state = initialOrganizationNameState, action: any) => {
    switch (action.type) {
        case actionsEnums.SET_ORGANIZATION_NAME:
            {
                return Object.assign({}, state, { organizationName: action.payload });
            }
    }

    return state;
}


