import { actionsEnums } from '../common/actionsEnums';

//--------------------

const initialCurrentPage = {
    currentPage: 1,
    numMembersPerPage: 12,
}

//--------------------

export const pageReducer = (state = initialCurrentPage, action: any) => {
    switch (action.type) {
        case actionsEnums.SET_CURRENT_PAGE:
            {
                return Object.assign({}, state, { currentPage: action.payload });
            }
    }

    return state;
}


