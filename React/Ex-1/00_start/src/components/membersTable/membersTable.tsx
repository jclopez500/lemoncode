import * as React from "react";
import { MemberEntity } from "../../model/member";
import { memberAPI } from "../../api/memberAPI";
import { MemberRow } from "./memberRow";
import { MemberHead } from "./memberHead";
import {} from "core-js";

interface Props {}

// We define members as a state (the compoment holding this will be a container
// component)
interface State {
  members: Array<MemberEntity>;
  organizationName: string;
  error: any;
}

// Nice tsx guide: https://github.com/Microsoft/TypeScript/wiki/JSX
export class MembersTableComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // set initial state
    this.state = {
      members: [],
      organizationName: "lemoncode",
      error: ""
    };
  }

  //--------------------------

  loadMembers = () => {
    memberAPI
      .getAllMembers(this.state.organizationName)
      .then(members => this.setState({ members: members, error: "" }))
      .catch(error => this.setState({ members: [], error: error }));
  };

  //--------------------------

  onChangeHandler = event => {
    this.setState({ organizationName: event.target.value });
  };

  //--------------------------

  onKeyUpHandler = event => {
    if (event.keyCode === 13) {
      this.loadMembers();
    }
  };

  //--------------------------

  public render() {
    let head = <h2> Members Page</h2>;

    //--------------------------

    let members = null;

    if (this.state.members.length > 0) {
      members = (
        <table className="table">
          <thead>
            <MemberHead />
          </thead>
          <tbody>
            {this.state.members.map((member: MemberEntity) => (
              <MemberRow key={member.id} member={member} />
            ))}
          </tbody>
        </table>
      );
    } else {
      this.state.error.message
        ? (members = <p style={{ color: "red" }}>{this.state.error.message}</p>)
        : null;
    }

    //--------------------------

    let input = (
      <input
        type="text"
        value={this.state.organizationName}
        onChange={event => this.onChangeHandler(event)}
        onKeyUp={this.onKeyUpHandler}
      />
    );

    //--------------------------

    let button = <button onClick={this.loadMembers}>Load</button>;

    //--------------------------

    return (
      <div className="row">
        {head}
        {input}
        {button}
        {members}
      </div>
    );
  }
}
