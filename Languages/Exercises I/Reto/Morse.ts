interface key { 
  [key: string]: string 
 }

//--------

class transmitter {

    private unit: number = 200;
    private sleepBetweenCode = this.unit;
    private sleepForCodeDot = this.unit * 1;
    private sleepForCodeDash = this.unit * 3;
    private sleepForChar = this.unit * 3;
    private sleepForWord = this.unit * 7;
    private globalCode: string = "";

    //--------


    //--------

    private convert(str: string): string {
       const msg: string = str.toLowerCase()
                              .replace(/\s+/g, " ")
                              .replace(/\s+/g, "|")
                              .trim()
                              .split("")
                              .map(char => this.morseAlphabet[char] || char)
                              .join(" ");
       return msg;
    };

    //--------

    private sleep(milliseconds: number) {
      const start = new Date().getTime();
      while ((new Date().getTime() - start) < milliseconds) {};
    }

    //--------

    private sendCode(code:string): void {
      this.globalCode += code;
      console.log(this.globalCode);
      this.sleep(code === '.' ? this.sleepForCodeDot : this.sleepForCodeDash);
      this.sleep(this.sleepBetweenCode);
    }

    //--------

    private sendChar(char: string) {
      this.globalCode += ' ';
      char.split("").forEach(code => this.sendCode(code))
      this.sleep(this.sleepForChar);
    }

    //--------

    private sendWord(word:string) {
      word.split(" ").forEach(char => this.sendChar(char));
      this.sleep(this.sleepForWord);
    }

    //--------

    public sendMsg(str: string, callback: () => void): void {
        console.log("msg -> " + str);
        const msg = this.convert(str);
        msg.split("|").forEach(word => this.sendWord(word));
        callback();
    }
    
    //--------

    private morseAlphabet:key = {
      "0": "-----",
      "1": ".----",
      "2": "..---",
      "3": "...--",
      "4": "....-",
      "5": ".....",
      "6": "-....",
      "7": "--...",
      "8": "---..",
      "9": "----.",
      "a": ".-",
      "b": "-...",
      "c": "-.-.",
      "d": "-..",
      "e": ".",
      "f": "..-.",
      "g": "--.",
      "h": "....",
      "i": "..",
      "j": ".---",
      "k": "-.-",
      "l": ".-..",
      "m": "--",
      "n": "-.",
      "o": "---",
      "p": ".--.",
      "q": "--.-",
      "r": ".-.",
      "s": "...",
      "t": "-",
      "u": "..-",
      "v": "...-",
      "w": ".--",
      "x": "-..-",
      "y": "-.--",
      "z": "--..",
      ".": ".-.-.-",
      ",": "--..--",
      "?": "..--..",
      "!": "-.-.--",
      "-": "-....-",
      "/": "-..-.",
      "@": ".--.-.",
      "(": "-.--.",
      ")": "-.--.-"
  };
}

//--------

function finish() {
  console.log("The message was sent!!!")
}

//--------

const t = new transmitter();
t.sendMsg("I need help", finish);