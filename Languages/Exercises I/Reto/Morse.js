"use strict";
//--------
var transmitter = /** @class */ (function () {
    function transmitter() {
        this.unit = 200;
        this.sleepBetweenCode = this.unit;
        this.sleepForCodeDot = this.unit * 1;
        this.sleepForCodeDash = this.unit * 3;
        this.sleepForChar = this.unit * 3;
        this.sleepForWord = this.unit * 7;
        this.globalCode = "";
        //--------
        this.morseAlphabet = {
            "0": "-----",
            "1": ".----",
            "2": "..---",
            "3": "...--",
            "4": "....-",
            "5": ".....",
            "6": "-....",
            "7": "--...",
            "8": "---..",
            "9": "----.",
            "a": ".-",
            "b": "-...",
            "c": "-.-.",
            "d": "-..",
            "e": ".",
            "f": "..-.",
            "g": "--.",
            "h": "....",
            "i": "..",
            "j": ".---",
            "k": "-.-",
            "l": ".-..",
            "m": "--",
            "n": "-.",
            "o": "---",
            "p": ".--.",
            "q": "--.-",
            "r": ".-.",
            "s": "...",
            "t": "-",
            "u": "..-",
            "v": "...-",
            "w": ".--",
            "x": "-..-",
            "y": "-.--",
            "z": "--..",
            ".": ".-.-.-",
            ",": "--..--",
            "?": "..--..",
            "!": "-.-.--",
            "-": "-....-",
            "/": "-..-.",
            "@": ".--.-.",
            "(": "-.--.",
            ")": "-.--.-"
        };
    }
    //--------
    //--------
    transmitter.prototype.convert = function (str) {
        var _this = this;
        var msg = str.toLowerCase()
            .replace(/\s+/g, " ")
            .replace(/\s+/g, "|")
            .trim()
            .split("")
            .map(function (char) { return _this.morseAlphabet[char] || char; })
            .join(" ");
        return msg;
    };
    ;
    //--------
    transmitter.prototype.sleep = function (milliseconds) {
        var start = new Date().getTime();
        while ((new Date().getTime() - start) < milliseconds) { }
        ;
    };
    //--------
    transmitter.prototype.sendCode = function (code) {
        this.globalCode += code;
        console.log(this.globalCode);
        this.sleep(code === '.' ? this.sleepForCodeDot : this.sleepForCodeDash);
        this.sleep(this.sleepBetweenCode);
    };
    //--------
    transmitter.prototype.sendChar = function (char) {
        var _this = this;
        this.globalCode += ' ';
        char.split("").forEach(function (code) { return _this.sendCode(code); });
        this.sleep(this.sleepForChar);
    };
    //--------
    transmitter.prototype.sendWord = function (word) {
        var _this = this;
        word.split(" ").forEach(function (char) { return _this.sendChar(char); });
        this.sleep(this.sleepForWord);
    };
    //--------
    transmitter.prototype.sendMsg = function (str, callback) {
        var _this = this;
        console.log("msg -> " + str);
        var msg = this.convert(str);
        msg.split("|").forEach(function (word) { return _this.sendWord(word); });
        callback();
    };
    return transmitter;
}());
//--------
function finish() {
    console.log("The message was sent!!!");
}
//--------
var t = new transmitter();
t.sendMsg("I need help", finish);
