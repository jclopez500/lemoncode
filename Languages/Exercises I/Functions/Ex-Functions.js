
//--- head function
const head = ([first]) => [first];

//--- tail function
const tail = ([, ...rest]) => rest;

//--- init function
const init = (array) =>  array.slice(0,array.length-1);

//--- last function
const last = (array) => array.slice(array.length-1);

//--- concat function
const concat = (a,b) => a.concat(b);

//--- concatN function
const concatN = (...arrs) => [].concat(...arrs);

//--- clone function
const clone = (obj) => JSON.parse(JSON.stringify(obj));

//--- merge function
const merge = (target, ...source) => Object.assign(target, ...source);

//--- isBookRead function
const isBookRead = (books,titleToSearch) =>  { 
    let book = books.find(book => book.title === titleToSearch);
    return book === undefined ? false : book.isRead;
};

//--- Class SlotMachine
class SlotMachine { 

    constructor() {
        this.coins = 0;
    }

    spinTheWheel() {
        let play = Math.floor(Math.random() * 7) + 1;
        return play.toString(2).padStart(3,"0");
    }


    play() {
        this.coins++;
        let play = this.spinTheWheel();
        if (play === '111') {
            console.log(`Congratulations!!!. You won ${this.coins} coins!!`);
            this.coins = 0;
        }
        else
            console.log('Good luck next time!!');
    }

};


//----------------------------------------------------
//--- Test
//----------------------------------------------------

const array = [1,2,3,4];

//--- head
console.log(head(array));

//--- tail
console.log(tail(array));

//--- init
console.log(init(array));

//--- last 
console.log(last(array));

//--- concat
const a = [1,2,3];
const b = [4,5,6];
console.log(concat(a,b));

//concatN
const c = [7,8,9];
const d = [10,11,12];
console.log(concatN(a,b,c,d));

//--- clone
const obj = {a:1, b:2, book: {title:"title", qty: 50}};
const objClone = clone(obj);
console.log (obj === objClone);
console.dir(objClone);

//--- merge
const target = {name: "Luisa", age: 31, married: true};
const source = {name: "Maria", surname: "Ibañez", country: "SPA"};

console.dir(merge(target,source));

//--- isBookRead
const books = [
    {title: "Harry Potter y la piedra filosofal", isRead: true},
    {title: "Canción de hielo y fuego", isRead: false},
    {title: "Devastación", isRead: true},
    ];
    
console.log(isBookRead(books, "Devastación"));
console.log(isBookRead(books, "Canción de hielo y fuego"));
console.log(isBookRead(books, "Los Pilares de la Tierra"));

//--- SlotMachine

const machine1 = new SlotMachine();
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 
machine1.play(); 