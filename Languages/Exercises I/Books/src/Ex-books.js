"use strict";
var books = [
    { title: "Harry Potter y la piedra filosofal", isRead: true },
    { title: "Canción de hielo y fuego", isRead: false },
    { title: "Devastación", isRead: true },
];
//--- isBookRead function
function isBookRead(books, titleToSearch) {
    var book = books.filter(function (book) { return book.title === titleToSearch; });
    if (book.length)
        return book[0].isRead;
    else
        return false;
}
;
//--- Test
console.log(isBookRead(books, "Devastación"));
console.log(isBookRead(books, "Canción de hielo y fuego"));
console.log(isBookRead(books, "Los Pilares de la Tierra"));
