interface  book  {
    title: string;
    isRead: boolean;
}

const books: book[] = [
    {title: "Harry Potter y la piedra filosofal", isRead: true},
    {title: "Canción de hielo y fuego", isRead: false},
    {title: "Devastación", isRead: true},
    ];

//--- isBookRead function
function isBookRead(books: book[], titleToSearch: string) : boolean  { 
    let book:book[] = books.filter((book:book) => book.title === titleToSearch);
    if (book.length) 
        return book[0].isRead;
    else
        return false;
};

//--- Test
console.log(isBookRead(books, "Devastación"));
console.log(isBookRead(books, "Canción de hielo y fuego"));
console.log(isBookRead(books, "Los Pilares de la Tierra"));


