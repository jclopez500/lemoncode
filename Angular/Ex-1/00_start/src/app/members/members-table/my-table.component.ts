import { Component, Input } from '@angular/core';
import { MemberEntity } from '../models/member.model';

@Component({
  selector: 'app-my-table',
  templateUrl: './my-table.component.html',
})
export class MyTableComponent {
  @Input() caption: string;
  @Input() members: MemberEntity;
  @Input() columns: string[];



}
