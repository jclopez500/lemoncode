import { Component } from '@angular/core';
import { MemberEntity } from '../models/member.model';
import { MembersApiService } from '../members-api.service';

@Component({
  selector: 'app-members-table',
  templateUrl: './members-table.component.html',
  styles: []
})
export class MembersTableComponent {

  members: MemberEntity[];
  organizationName = 'lemoncode';
  errorMessage = '';
  error = false;

  constructor(private membersApi: MembersApiService) { }

  loadMembers() {
    this.error = false;
    this.errorMessage = '';

    this.membersApi.getAllMembers(this.organizationName)
      .subscribe(
        data => this.members = <MemberEntity[]>data,
        error => {
          this.error = true;
          this.errorMessage = '"' + this.organizationName + '"' + " doesn't exist";
        });
  }

  onEnter(value: string) {
    this.organizationName = value;
  }

}
