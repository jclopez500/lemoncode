import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MembersTableComponent } from './members-table';

import { MyTableComponent } from './members-table/my-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
  ],
  declarations: [
    MyTableComponent,
    MembersTableComponent
  ],
  exports: [
    MembersTableComponent
  ]
})
export class MembersModule { }
